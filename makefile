.VERSION=1.0.0

all:
	${MAKE} build

build:
	${MAKE} gogets
	${MAKE} clean
	godep go build ${.LDFLAGS} ./...
	${MAKE} test

godep:
	rm -fr Godeps
	rm -fr vendor
	godep save ./...

test:
	go test -cover ./...

clean:
	godep go clean

gogets:
	go get github.com/tools/godep
	go get -d -t ./...
