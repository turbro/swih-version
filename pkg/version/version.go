package version

import (
	"github.com/gorilla/mux"
	"encoding/json"
	"net/http"
	"io"
)

var GIT_HASH string
var TIMESTAMP string
var VERSION string
var MAKEFILE string

type Build struct {

}

func AddRouter(router *mux.Router, contextRoot string) {
	router.HandleFunc(contextRoot+"/version", display).Methods(http.MethodGet)
}

func display(writer http.ResponseWriter, reader *http.Request) {
	io.WriteString(writer, GetVersion())
}

func GetVersion() string {
	mapD := map[string]string{
		"VERSION":VERSION,
		"TIMESTAMP": TIMESTAMP,
		"GIT_HASH": GIT_HASH,
		"MAKEFILE": MAKEFILE}

	jsonPrettyPrint, _ := json.MarshalIndent(mapD,"", "    \n")
	return string(jsonPrettyPrint)
}
